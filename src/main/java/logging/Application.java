package logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger defaultLogger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        defaultLogger.trace("This is a trace message");
        defaultLogger.debug("This is a debug message");
        defaultLogger.info("This is an info message");
        defaultLogger.warn("This is a warn message");
        defaultLogger.error("This is an error message");
        defaultLogger.fatal("This is a fatal message");
    }
}
